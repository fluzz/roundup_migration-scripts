import sys
import re
from datetime import datetime, timezone
from enum import Enum
import zipfile

# for roundup download
import roundup
from roundup import admin
import ast

# for gitea upload
import requests
import json
import os
from pygments import highlight
from pygments.formatters.terminal256 import Terminal256Formatter
from pygments.lexers.web import JsonLexer

Debug = Enum('Debug', ['ALWAYS', 'LOAD', 'HISTORY', 'REVERT', 'UPLOAD', 'REQUEST'])
DebugLvl = (Debug.ALWAYS, Debug.ALWAYS)
DebugLvl = (Debug.ALWAYS, Debug.LOAD, Debug.HISTORY, Debug.UPLOAD, Debug.REQUEST)

DryRun = False
ForceUpload = True

AllowedUpload = [ '.csv', '.docx', '.fodg', '.fodp', '.fods', '.fodt', '.gif', '.gz', '.jpeg', '.jpg', '.log', '.md', '.mov', '.mp4', '.odf', '.odg', '.odp', '.ods', '.odt', '.pdf', '.png', '.pptx', '.svg', '.tgz', '.txt', '.webm', '.xls', '.xlsx', '.zip' ]

issuelist = None
#issuelist = [ '200', '237', '503', '602', '804', '951', '952', '953', '967', '968' ]
issuelist = [ '965' ]

def debug(kind, str):
    if kind in DebugLvl:
        print(str)
        
def error(str):
    print('>>>> ERROR <<<<')
    print('  ' + str)
    quit()

#====================================================================
# FOR ROUNDUP
#====================================================================

adm = admin.AdminTool()
adm.tracker_home = "./roundup"
adm.name = 'admin'
adm.password = ''
adm.separator = None
adm.print_designator = 0
adm.verbose = 0

tracker = roundup.instance.open(adm.tracker_home)
adm.db = tracker.open(adm.name)

fileDir = "./roundup/db/files/file/0"

#====================================================================
# FOR GITEA
#====================================================================

webhookUrl = 'http://notifico.vm.net:3000/api/v1/repos/sam/test2'
api_token = '18b6d007de4dcfd95b00f0457757bd28c0f486bc'

#====================================================================
# HELPERS
#====================================================================

def dump_resp(resp, issue):
    debug(Debug.REQUEST, resp)
      
    if resp.status_code == 200 or resp.status_code == 201:  # success with response
        if Debug.REQUEST in DebugLvl:
            raw_json = json.dumps(resp.json(), sort_keys=True, indent=2)
            colorful = highlight(
                raw_json,
                lexer=JsonLexer(),
                formatter=Terminal256Formatter(),
            )
            print(colorful)
    elif resp.status_code == 204: # sucess with empty response
        pass
    else: # error
        print("Failed request for issue #{} (code {}):".format(issue.id, resp.status_code))
        raw_json = json.dumps(resp.json(), sort_keys=True, indent=2)
        colorful = highlight(
            raw_json,
            lexer=JsonLexer(),
            formatter=Terminal256Formatter(),
        )
        print(colorful)
        sys.exit(1)


def reformat_msg(body, author, issue, with_link):
    msg = ''
    if Users[author].xname == DefaultUser:
        # check if the initial author is already mentionned
        # using "Submitted by xxxxx\n"
        if body is not None and body.startswith("Submitted by") :
            msg = re.sub(r'Submitted by ([^\n]*)\n', r'Initially submitted by \1\n\n-----\n', body)
        else :
            msg = 'Initially submitted by {}\n\n-----\n'.format(Users[author].username)
            msg += body if body is not None else ''
    else:
        msg = body
    msg = re.sub(r'issue([0-9]+)', r'#\1', msg)
    #if with_link and len(issue.linked) != 0:
    #    msg += '\n\n-----\nSee also: #{}'.format(", #".join(issue.linked))
    return msg


def upload_file(issue, from_msg, fileid):
    file = Files[fileid]

    if os.path.splitext(file.name)[1] in AllowedUpload:
        upload_zip = False
        filename = file.name
        att_file = "{}/file{}".format(fileDir, fileid)
    else:
        upload_zip = True
        att_origfile = "{}/file{}".format(fileDir, fileid)
        filename = file.name + ".zip"
        att_file = att_origfile + ".zip"
        with zipfile.ZipFile(att_file, 'w') as ziped:
            ziped.write(att_origfile)

    if from_msg is None:
        # If the file is not associated to a message, register it to the issue
        verb = "/issues/{}/assets".format(issue.id)
        posterid = Users[issue.creator].xid
        created_at = issue.creation.isoformat()
    else:
        commentid = from_msg.refs[0]
        verb = "/issues/comments/{}/assets".format(commentid)
        posterid = Users[from_msg.actor].xid
        created_at = from_msg.date.isoformat()

    dataset = {
        'name' : filename,
        'posterid' : posterid,
        'created_at' : created_at
    }
    files = {
        'attachment': (os.path.basename(att_file), open(att_file, 'rb'), 'application/octet-stream')
    }
    debug(Debug.UPLOAD, "{} : {} - {}".format(verb, json.dumps(dataset), att_file))
    if not DryRun:
        req = requests.post(webhookUrl + verb, params=dataset, files=files, headers={'Authorization' : 'token '+api_token})
    if upload_zip:
        os.remove(att_file)


#====================================================================
# Translation Tables
#====================================================================

DefaultUser = 'tracker_migration'
DefaultUserId = '7' # 'tracker_migration' on roundup
DefaultUserXId = 4 # 'tracker_migration' on codeberg

UserTranslation = {
	DefaultUser : { 'name' : DefaultUser, 'id' : DefaultUserXId },
	'fluzz' : { 'name' : 'fluzz', 'id' : 3 },
	'jesusalva' : { 'name' : 'jesusalva', 'id' : 5 },
	'ahuillet' : { 'name' : 'ahuillet', 'id' : 6 }
}

Collabs = []   # Filled during pre-run

def adjust_poster(poster):
    if poster.xname in Collabs:
        return poster
    else:
        return Users[DefaultUserXId]

StatusTranslation = {
	'open' : { 'closed' : False },
	'resolved' : { 'closed' : True },
	'rejected' : { 'closed' : True },
	'postponed' : { 'closed' : False }
}

LabelStart = 67

KeywordTranslation = {
	'opengl' : { 'id' : LabelStart },             # 1
	'editor' : { 'id' : LabelStart + 1 },         # 2
	'sdl' : { 'id' : LabelStart + 2 },            # 3
	'dialog' : { 'id' : LabelStart + 3 },         # 4
	'mapgen' : { 'id' : LabelStart + 4 },         # 5
	'quest' : { 'id' : LabelStart + 5 },          # 7
	'map' : { 'id' : LabelStart + 6 },            # 8
	'graphics' : { 'id' : LabelStart + 7 },       # 9
	'takeover' : { 'id' : LabelStart + 8 },       # 10
	'translation' : { 'id' : LabelStart + 9 },    # 11
	'documentation' : { 'id' : LabelStart + 10 }  # 12
}

# Priority is transformed into a keyword
PriorityTranslation = {
	'critical' : { 'id' : LabelStart + 11 },         # 1
	'important' : { 'id' : LabelStart + 12 },        # 2
	'bug' : { 'id' : LabelStart + 13 },              # 3
	'minor' : { 'id' : LabelStart + 14 },            # 4
	'feature' : { 'id' : LabelStart + 15 },          # 5
	'idea' : { 'id' : LabelStart + 16 },             # 6
	'release-blocker' : { 'id' : LabelStart + 17 },  # 7
	'0.16 rc bugs' : { 'id' : LabelStart + 18 }      # 8
}

#====================================================================
#====================================================================

class Base:

    def __init__(self, node, attrs):
        for attr in attrs:
            if attr[0] != '_':
                setattr(self, attr, node[attr])

    def __repr__(self):
        repr = []
        for attr, value in self.__dict__.items():
            if attr[0] != '_':
                repr.append('{}: {}'.format(attr, value))
        return ',\n '.join(repr)

    @classmethod
    def load(cls):
        items = {}
        ids = adm.db.getclass(cls.db_name)
        for id in ids.list():
            table = getattr(adm.db, cls.db_name)
            node = table.getnode(id)
            item = cls(node)
            items[node['id']] = item
        return items

#======
# Get users
#======

class User(Base):
    db_name = 'user'

    def __init__(self, node):
        self.id = '-1'
        self.username = None
        self.realname = None
        self.address = None
        Base.__init__(self, node, self.__dict__.keys())
        if self.username in UserTranslation.keys():
            self.xname = UserTranslation[self.username]['name']
            self.xid = UserTranslation[self.username]['id']
            self.is_known = True
        else:
            self.xname = UserTranslation[DefaultUser]['name']
            self.xid = UserTranslation[DefaultUser]['id']
            self.is_known = False
    
Users = User.load()

#debug(Debug.LOAD, Users['4'])
#debug(Debug.LOAD, '-------------------------------------------')

#=====
# Get keywords
#=====

class Keyword(Base):
    db_name = 'keyword'

    def __init__(self, node):
        self.id = '-1'
        self.name = None
        Base.__init__(self, node, self.__dict__.keys())
        self.xid = KeywordTranslation[self.name]['id']

Keywords = Keyword.load()

#debug(Debug.LOAD, Keywords['5'])
#debug(Debug.LOAD, '-------------------------------------------')

#=====
# Get priorities
#=====

class Priority(Base):
    db_name = 'priority'

    def __init__(self, node):
        self.id = '-1'
        self.name = None
        self.order = None
        Base.__init__(self, node, self.__dict__.keys())
        self.xid = PriorityTranslation[self.name]['id']

Priorities = Priority.load()

#debug(Debug.LOAD, Priorities['7'])
#debug(Debug.LOAD, '-------------------------------------------')

#=====
# Get status
#=====

class Status(Base):
    db_name = 'status'

    def __init__(self, node):
        self.id = '-1'
        self.name = None
        self.order = None
        Base.__init__(self, node, self.__dict__.keys())
        self.isClosed = StatusTranslation[self.name]['closed']


Statuses = Status.load()

#debug(Debug.LOAD, Statuses['2'])
#debug(Debug.LOAD, '-------------------------------------------')

#=====
# Get files
#=====

class File(Base):
    db_name = 'file'

    def __init__(self, node):
        self.id = '-1'
        self.name = None
        self.type = None
        Base.__init__(self, node, self.__dict__.keys())


Files = File.load()

#debug(Debug.LOAD, Files['277'])
#debug(Debug.LOAD, '-------------------------------------------')

#=====
# Get messages
#=====

class Msg(Base):
    db_name = 'msg'

    def __init__(self, node):
        self.id = '-1'
        self.content = None
        Base.__init__(self, node, self.__dict__.keys())
        self.content = re.sub(r"\r\n", "\n", self.content)
        self.content = re.sub(r'issue[\s]*([0-9]+)', r'issue\1', self.content)
        self.content = re.sub(r'http://bugs.freedroid.org/b/issue([0-9]+)', r'issue\1', self.content)
        self.content = re.sub(r'https://bugs.freedroid.org/b/issue([0-9]+)', r'issue\1', self.content)


Msgs = Msg.load()

#debug(Debug.LOAD, Msgs['1233'])
#debug(Debug.LOAD, '-------------------------------------------')

#=====
# Get issues
#=====

class Patch:

    def __init__(self, ref_name, patch, actor, date):
        self.ref_name = ref_name
        self.actor = actor if actor in Users.keys() else DefaultUserId
        ts = date.timestamp()
        self.date = datetime.fromtimestamp(ts, tz=timezone.utc)
        self.active = True
        
    def __repr__(self):
        return "'who': {} ({}) -> {} ({}), 'date': {}".format(self.actor, Users[self.actor].username, Users[self.actor].xid, Users[self.actor].xname, self.date.isoformat())

    def inactivate(self):
        self.active = False

    def revert(self, issue):
        pass

    def remove_in_patch(self, other):
        return False
        
    def remove_in_issue(self, issue):
        pass

    def get_assignees(self):
        return None

    def get_links(self):
        return (None, None)

    def upload(self, issue):
        pass        

class OldNewPatch(Patch):

    def __init__(self, ref_name, patch, actor, date, issue):
        Patch.__init__(self, ref_name, patch, actor, date)
        self.oldvalue = patch
        self.newvalue = getattr(issue, ref_name)

    def revert(self, issue):
        setattr(issue, self.ref_name, self.oldvalue)

    def __repr__(self):
        oldtxt = "'" + self.oldvalue  + "'" if self.oldvalue else 'None' 
        newtxt = "'" + self.newvalue  + "'" if self.newvalue else 'None' 
        return Patch.__repr__(self) + " - {'" + self.ref_name + "': " + oldtxt + " -> " + newtxt + "}"
        

class MultiPatch(Patch):

    def __init__(self, ref_name, patch, actor, date, issue):
        Patch.__init__(self, ref_name, patch, actor, date)
        self.direction = ''
        self.refs = []
        self.ref_name = ref_name
        direction, refs = patch
        if not direction in ('+', '-') and type(patch) == 'tuple':
            error('Unknown direction in "{}"'.format(patch))
        elif type(patch) == 'array':
            self.direction = '+'
            self.refs = patch
        else:
            self.direction = direction
            self.refs = refs

    def __repr__(self):
        return Patch.__repr__(self) + " - {'" + self.ref_name + "': ('" + self.direction + "', " + str(self.refs) + ")}"

    def remove_in_patch(self, other):
        if type(self).__name__ == type(other).__name__ and \
           self.direction != other.direction and \
           self.direction == '-' :
            new_refs = [ ref for ref in other.refs if ref not in self.refs ]
            if len(new_refs) != len(other.refs):
                other.refs = new_refs
                if len(other.refs) == 0:
                    other.inactivate()
                self.inactivate()
                return True
        return False

    def remove_in_issue(self, issue):
        if self.direction == '-':
            new_refs = [ ref for ref in getattr(issue, self.ref_name) if ref not in self.refs ]
            if len(new_refs) != len(getattr(issue, self.ref_name)):
                setattr(issue, self.ref_name, new_refs)
                self.inactivate()

    def revert(self, issue):
        if self.direction == '+':
            for ref in self.refs:
                getattr(issue, self.ref_name).remove(ref)
        elif self.direction == '-':
            for ref in self.refs:
                getattr(issue, self.ref_name).append(ref)


class CreatePatch(Patch):

    def __init__(self, patch, actor, date, issue):
        Patch.__init__(self, 'create', patch, actor, date)
        self.issue = issue
        if issue.actor not in Users:
            issue.actor = DefaultUserId
        if issue.assignedto not in Users:
            issue.assignedto = DefaultUserId

    def __repr__(self):
        return Patch.__repr__(self) + " - {'" + self.ref_name + "')}"

    def upload(self, issue):
        # Add issue
        verb = "/issues"
        body = ''
        if len(issue.messages) != 0 and issue.messages[0] in Msgs:
            body = Msgs[issue.messages[0]].content
        msg = reformat_msg(body, issue.creator, issue, True)
        labels = []
        for keyword in issue.keyword:
            if keyword in Keywords:
                labels += [ Keywords[keyword].xid ]
        if issue.priority and issue.priority in Keywords:
            labels += [ Priorities[issue.priority].xid ]
        assignedto = adjust_poster(Users[issue.assignedto])
        if assignedto.xname != DefaultUser:
            assignees = [ assignedto.xname ]
        else:
            assignees = []
        dataset = {
            'assignees' : assignees,
            'index' : int(issue.id),
            'title' : issue.title,
            'body' : msg,
            'posterid' : Users[issue.creator].xid,
            'created_at' : issue.creation.isoformat(),
            'closed' : Statuses[issue.status].isClosed,
            #'due_date' : string($date-time),
            'labels' : labels,
            #'milestone' : 1,
            #'ref' : string,
        }
        debug(Debug.UPLOAD, "{} : {}".format(verb,json.dumps(dataset)))
        if not DryRun:
            req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
            dump_resp(req, issue)

	# TODO : set priority
	
        if len(issue.files) != 0 :
            for fileid in issue.files:
                upload_file(issue, None, fileid)

    def get_links(self):
        msg_links = []
        if len(self.issue.messages) != 0 and self.issue.messages[0] in Msgs:
            msg = Msgs[self.issue.messages[0]].content
            links = re.findall(r'issue[0-9]+', msg)
            for link in links:
                issueid = re.match(r'issue([0-9]+)', link).groups()[0]
                msg_links += [issueid]
        return (None, msg_links)

    
class LinkPatch(MultiPatch):

    def __init__(self, patch, actor, date, issue):
        MultiPatch.__init__(self, 'linked', patch, actor, date, issue)

    def get_links(self):
        return (self.refs, None)


class MsgPatch(MultiPatch):

    def __init__(self, patch, actor, date, issue):
        MultiPatch.__init__(self, 'messages', patch, actor, date, issue)

    def upload(self, issue):
        for ref in self.refs:
            if ref not in Msgs:
                continue
            verb = "/issues/{}/comments".format(issue.id)
            msg = reformat_msg(Msgs[ref].content, self.actor, issue, True)
            dataset = {
                'body' : msg,
                'posterid' : Users[self.actor].xid,
                'created_at' : self.date.isoformat()
            }
            debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
            if not DryRun:
                req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
                dump_resp(req, issue)

    def get_links(self):
        msg_links = []
        for ref in self.refs:
            if ref not in Msgs:
                continue
            msg = Msgs[ref].content
            links = re.findall(r'issue[0-9]+', msg)
            for link in links:
                issueid = re.match(r'issue([0-9]+)', link).groups()[0]
                msg_links += [issueid]
        return (None, msg_links)


class TitlePatch(OldNewPatch):

    def __init__(self, patch, actor, date, issue):
        OldNewPatch.__init__(self, 'title', patch, actor, date, issue)

    def remove_in_issue(self, issue):
        issue.title = self.newvalue
        self.inactivate()


class StatusPatch(OldNewPatch):

    def __init__(self, patch, actor, date, issue):
        OldNewPatch.__init__(self, 'status', patch, actor, date, issue)

    def upload(self, issue):
        verb = "/issues/{}".format(issue.id)
        if self.newvalue is None:
            dataset = {
                'state' : 'open',
                'posterid' : Users[self.actor].xid,
                'created_at' : self.date.isoformat()
            }            
        else:
            dataset = {
                'state' : 'closed' if Statuses[self.newvalue].isClosed else 'open',
                'posterid' : Users[self.actor].xid,
                'closed_at' : self.date.isoformat() if Statuses[self.newvalue].isClosed else None,
                'created_at' : self.date.isoformat()
            }
        debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
        if not DryRun:
            req = requests.patch(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
            dump_resp(req, issue)


class AssignPatch(OldNewPatch):

    def __init__(self, patch, actor, date, issue):
        OldNewPatch.__init__(self, 'assignedto', patch, actor, date, issue)

    def get_assignees(self):
        if self.newvalue is not None:
            return Users[self.newvalue].username
        else:
            return None
 
    def upload(self, issue):
        if self.newvalue is None:
            return
        
        poster = adjust_poster(Users[self.newvalue])
        if poster.xname != DefaultUser:
            verb = "/issues/{}".format(issue.id)
            dataset = {
                'assignees' : [ poster.xname ],
                'posterid' : poster.xid,
                'created_at' : self.date.isoformat()
            }
            debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
            if not DryRun:
                req = requests.patch(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
                dump_resp(req, issue)


class NosyPatch(MultiPatch):

    def __init__(self, patch, actor, date, issue):
        MultiPatch.__init__(self, 'nosy', patch, actor, date, issue)

    def upload(self, issue):
        # Is there something equivalent on forgejo
        pass


class PrioPatch(OldNewPatch):

    def __init__(self, patch, actor, date, issue):
        OldNewPatch.__init__(self, 'priority', patch, actor, date, issue)

    def upload(self, issue):
        # Remove old priority label
        if self.oldvalue in Priorities:
            verb = "/issues/{}/labels/{}".format(issue.id, Priorities[self.oldvalue].xid)
            debug(Debug.UPLOAD, "{}".format(verb))
            if not DryRun:
                req = requests.delete(webhookUrl + verb, headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
                dump_resp(req, issue)
    
        # Add new priority label
        if self.newvalue in Priorities:
            verb = "/issues/{}/labels".format(issue.id)
            labels = [ Priorities[self.newvalue].xid ]
            dataset = {
                'labels' : labels,
                'posterid' : Users[self.actor].xid,
                'created_at' : self.date.isoformat()
            }
            debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
            if not DryRun:
                req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
                dump_resp(req, issue)

class KeywordPatch(MultiPatch):

    def __init__(self, patch, actor, date, issue):
        MultiPatch.__init__(self, 'keyword', patch, actor, date, issue)
        checked_refs = []
        for ref in self.refs:
            if ref in Keywords:
                checked_refs += [ ref ]
        self.refs = checked_refs
        if len(self.refs) == 0:
            self.active = False
 
    def upload(self, issue):
        verb = "/issues/{}/labels".format(issue.id)
        labels = [ Keywords[label].xid for label in self.refs ]
        dataset = {
            'labels' : labels,
            'posterid' : Users[self.actor].xid,
            'created_at' : self.date.isoformat()
        }
        debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
        if not DryRun:
            req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
            dump_resp(req, issue)


class FilePatch(MultiPatch):

    def __init__(self, patch, actor, date, issue):
        MultiPatch.__init__(self, 'files', patch, actor, date, issue)
        self.from_msg = None
        
    def set_from_msg(self, msg):
        self.from_msg = msg

    def upload(self, issue):
        for fileid in self.refs:
            upload_file(issue, self.from_msg, fileid)


class HistoryStep:
    
    def __init__(self, step, issue):
        self.issue_id, self.date, self.actor, self.action, self.data = step
        self.patches = []
        self.valid = False
        
        if self.action in ('link'):
            return
            
        if self.action == 'create':
            self.patches.append(CreatePatch(None, self.actor, self.date, issue))
            self.valid = True
            return
            
        if self.action != 'set':
            error('unkown action in: ' + hist_step)
            return
        
        # Used to associate a file with a message
        # If several messages are listed in the current step (should it happen ??)
        # we keep the last one
        has_msg = None
        has_file = []
        
        for kind, data in self.data.items():
            if kind == 'linked':
                for action in data:
                    self.patches.append(LinkPatch(action, self.actor, self.date, issue))
                    self.valid = True
                continue
            if kind == 'messages':
                for action in data:
                    msgPatch = MsgPatch(action, self.actor, self.date, issue)
                    self.patches.append(msgPatch)
                    self.valid = True
                    has_msg = msgPatch
                continue
            if kind == 'title':
                self.patches.append(TitlePatch(data, self.actor, self.date, issue))
                self.valid = True
                continue
            if kind == 'status':
                self.patches.append(StatusPatch(data, self.actor, self.date, issue))
                self.valid = True
                continue
            if kind == 'assignedto':
                self.patches.append(AssignPatch(data, self.actor, self.date, issue))
                self.valid = True
                continue
            if kind == 'nosy':
                for action in data:
                    self.patches.append(NosyPatch(action, self.actor, self.date, issue))
                    self.valid = True
                continue
            if kind == 'priority':
                self.patches.append(PrioPatch(data, self.actor, self.date, issue))
                self.valid = True
                continue
            if kind == 'keyword':
                for action in data:
                    self.patches.append(KeywordPatch(action, self.actor, self.date, issue))
                    self.valid = True
                continue
            if kind == 'files':
                for action in data:
                    filePatch = FilePatch(action, self.actor, self.date, issue)
                    self.patches.append(filePatch)
                    self.valid = True
                    has_file.append(filePatch)
                continue
            error("UNHANDLED action '{}': {}".format(kind, data))

        # Status patch should be at the end of the set, to be displayed after all
        # the other patches
        status_patches = []
        for patch in self.patches:
            if type(patch).__name__ == 'StatusPatch':
                status_patches.append(patch)
                self.patches.remove(patch)
        self.patches = self.patches + status_patches
                
        if has_msg is not None and len(has_file) != 0:
            for file in has_file:
                file.set_from_msg(has_msg)

    def get_patches(self):
        if self.valid:
            return self.patches
        else:
            return []

    def __repr__(self):
        return "[" + self.action + "]\n" + "\n".join([ " "+str(patch) for patch in self.get_patches() ]) 


class Issue(Base):
    db_name = 'issue'

    def __init__(self, node):
        self._history = []
        self.id = '-1'
        self.creator = None
        self.creation = None
        self.actor = None
        self.activity = None
        self.title = None
        self.messages = None
        self.priority = None
        self.status = None
        self.assignedto = None
        self.nosy = []
        self.files = []
        self.keyword = []
        self.linked = []
        Base.__init__(self, node, self.__dict__.keys())
        if self.creator not in Users.keys():
            self.creator = DefaultUserId
        ts = self.creation.timestamp()
        self.creation = datetime.fromtimestamp(ts, tz=timezone.utc)
        self.title = re.sub(r"\r\n", "\n", self.title)

    def add_history_step(self, data):
        hist_step = HistoryStep(data, self)
        debug(Debug.HISTORY, hist_step)
        for patch in reversed(hist_step.get_patches()):
            if patch.active:
                debug(Debug.REVERT, 'revert: {}'.format(patch))
                patch.revert(self)
                self._history.insert(0, patch)

    def clean_history(self):
        for index in range(0, len(self._history)):
            found_in_history = False
            for ptr in range(0, index+1):
                if self._history[index].remove_in_patch(self._history[ptr]):
                    found_in_history = True
                    break
            if not found_in_history:
                self._history[index].remove_in_issue(self)            

    def dump_history(self):
        return "\n".join([ str(patch) for patch in self._history if patch.active is True ])

    def get_assignees(self):
        pass
        
    def get_assignees(self):
        assignees = []
        for step in self._history:
            if step.active:
                assignee = step.get_assignees()
                if assignee is not None:
                    assignees += [assignee]
        return assignees
        
    def get_links(self):
        msg_all_links = []
        issue_all_links = []
        for step in self._history:
            if step.active:
                (step_links, msg_links) = step.get_links()
                if step_links is not None:
                    issue_all_links += step_links
                if msg_links is not None:
                    msg_all_links += msg_links
        self.linked = [ i for i in issue_all_links if i not in msg_all_links ]

    def add_see_also(self):
        self.get_links()
        see_also = [ i for i in self.linked if i < self.id ]
        if len(see_also) != 0:
            verb = "/issues/{}/comments".format(self.id)
            msg = 'See also #{}'.format(", #".join(see_also))
            dataset = {
                'body' : msg,
                'posterid' : Users[self.creator].xid,
                'created_at' : self.creation.isoformat(),
            }
            debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
            if not DryRun:
                req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
                dump_resp(req, issue)

      
    def upload_create(self):
        for step in self._history:
            if step.active and type(step).__name__ == 'CreatePatch':
                step.upload(self)

    def upload_other(self):
        for step in self._history:
            if step.active and type(step).__name__ != 'CreatePatch':
                step.upload(self)


Issues = Issue.load()

#======================== PRE-RUN =====================================

debug(Debug.LOAD, '======= GET COLLABORATORS ==========================')
Collabs = ['sam']
verb = "/collaborators"
if not DryRun:
    req = requests.get(webhookUrl + verb, headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
    Collabs += [ collab['username'] for collab in req.json() ]
else:
    Collabs += [DefaultUser, 'fluzz', 'ahuillet', 'jesusalva']
debug(Debug.LOAD, Collabs)

if not DefaultUser in Collabs:
    print(DefaultUser + " MUST be a collaborator. Can not proceed...")
    sys.exit(1)

#======================== PROCEED =====================================

issuelist = Issues.keys() if issuelist is None else issuelist

for issueid in issuelist:
    issue = Issues[issueid]

    debug(Debug.LOAD, '====================================================')
    debug(Debug.LOAD, issue)

    for hist_step in reversed(adm.db.getclass('issue').history(issueid)):
        issue.add_history_step(hist_step)

    debug(Debug.LOAD, '======= BEFORE CLEAN ==================================')
    debug(Debug.LOAD, issue)

    debug(Debug.HISTORY, '........................................................')
    debug(Debug.HISTORY, issue.dump_history())

    issue.clean_history()

    debug(Debug.LOAD, '======= AFTER CLEAN ===================================')
    debug(Debug.LOAD, issue)

    debug(Debug.HISTORY, '.........................................................')
    debug(Debug.HISTORY, issue.dump_history())


#====
# Check if collaborators must be added in order to assign them an issue

all_assignees = []
for issueid in Issues.keys():
    assignees = Issues[issueid].get_assignees()
    if len(assignees) != 0:
        all_assignees += assignees
        
known_collabs = {}
unknown_collabs = []
missing_collabs = {}
for assignee in sorted(set(all_assignees)):
    if assignee in UserTranslation:
        xname = UserTranslation[assignee]['name']
        if xname in Collabs:
            known_collabs[assignee] = UserTranslation[assignee]
        else:
            missing_collabs[assignee] = UserTranslation[assignee]
    else:
        unknown_collabs += [assignee]

debug(Debug.LOAD, "***** KNOWN COLLABS: {}".format(known_collabs))
debug(Debug.LOAD, "***** MISSING COLLABS: {}".format(missing_collabs))
debug(Debug.LOAD, "***** UNKOWN COLLABS: {}".format(unknown_collabs))

if len(missing_collabs) != 0:
    print("==================================================")
    print("You must add the following collaborators, in order to assign them issues:")
    for collab in missing_collabs.items():
        print("  '{}', known has '{}' (#{}) in codeberg".format(collab[0], collab[1]['name'], collab[1]['id']))

if not ForceUpload and len(unknown_collabs) != 0:
    print("==================================================")
    print("Those people are unknown to codeberg, but have issues assign to them.")
    print("Set 'ForceUpload' to True if you want to proceed anyway.")
    print("  " + ", ".join(unknown_collabs))
    if not DryRun:
        sys.exit(1)


#====
# Upload issues 
# Do not factorize into one single loop !
# We need to first create the issue, so that they can be refered to
# in some later comments

for issueid in issuelist:
    issue = Issues[issueid]
    debug(Debug.ALWAYS, '===== Create #{}'.format(issueid))
    issue.upload_create()

for issueid in issuelist:
    debug(Debug.ALWAYS, '===== See-Also #{}'.format(issueid))
    Issues[issueid].add_see_also()

for issueid in issuelist:
    issue = Issues[issueid]
    debug(Debug.ALWAYS, '===== Comment #{}'.format(issueid))
    issue.upload_other()


quit()

#---------------------------------

#ns = adm.db.issue.getnodeids()
#for nodeid in ns:
#cl = adm.db.getclass('issue')
#for nodeid in cl.list():
#  title = adm.db.issue.get(nodeid, 'title')
#  print('%s: %s' % (nodeid, title)) 
#
#
#print('==========================================================')

#cl = adm.db.getclass('issue')
#keys = sorted(cl.getprops().keys())
#for key in keys:
#  value = cl.get(issueid, key)
#  if key == 'creator':
#    print('%s: %s (%s)' % (key, value, adm.db.user.get(value, 'username')))
#  else:
#    print('%s: %s' % (key, value))
#  
#print('==========================================================')

#node = adm.db.getnode('issue', issueid)
#node = adm.db.issue.getnode(issueid)
#for key, value in node.items():
#  if key == 'creator':
#    print('%s: %s (%s)' % (key, value, adm.db.user.get(value, 'username')))
#  else:
#    print('%s: %s' % (key, value))

#print('==========================================================')

#cl = adm.db.getclass('issue').history(issueid)
#for hist in reversed(cl):
#  print(hist)

