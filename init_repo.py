import sys
from enum import Enum

# for gitea upload
import requests
import json
import os
from pygments import highlight
from pygments.formatters.terminal256 import Terminal256Formatter
from pygments.lexers.web import JsonLexer

Debug = Enum('Debug', ['ALWAYS', 'LOAD', 'HISTORY', 'REVERT', 'UPLOAD', 'REQUEST'])
DebugLvl = (Debug.ALWAYS, Debug.ALWAYS)
DebugLvl = (Debug.ALWAYS, Debug.LOAD, Debug.HISTORY, Debug.UPLOAD, Debug.REQUEST)

DryRun = False

def debug(kind, str):
    if kind in DebugLvl:
        print(str)
        
def error(str):
    print('>>>> ERROR <<<<')
    print('  ' + str)
    quit()

#====================================================================
# FOR GITEA
#====================================================================

webhookUrl = 'http://notifico.vm.net:3000/api/v1/repos/sam/test2'
api_token = '18b6d007de4dcfd95b00f0457757bd28c0f486bc'

#====================================================================
# HELPERS
#====================================================================

def dump_resp(resp):
    debug(Debug.REQUEST, resp)
      
    if resp.status_code == 200 or resp.status_code == 201:  # success with response
        if Debug.REQUEST in DebugLvl:
            raw_json = json.dumps(resp.json(), sort_keys=True, indent=2)
            colorful = highlight(
                raw_json,
                lexer=JsonLexer(),
                formatter=Terminal256Formatter(),
            )
            print(colorful)
    elif resp.status_code == 204: # sucess with empty response
        pass
    else: # error
        print("Failed request (code {}):".format(resp.status_code))
        raw_json = json.dumps(resp.json(), sort_keys=True, indent=2)
        colorful = highlight(
            raw_json,
            lexer=JsonLexer(),
            formatter=Terminal256Formatter(),
        )
        print(colorful)
        sys.exit(1)

#====================================================================
# INIT COLLABS
#====================================================================

DefaultUser = 'tracker_migration'
DefaultUserId = '7' # 'tracker_migration' on roundup
DefaultUserXId = 4 # 'tracker_migration' on codeberg

UserTranslation = {
	DefaultUser : { 'name' : DefaultUser, 'id' : DefaultUserXId },
	'fluzz' : { 'name' : 'fluzz', 'id' : 3 },
	'jesusalva' : { 'name' : 'jesusalva', 'id' : 5 },
	'ahuillet' : { 'name' : 'ahuillet', 'id' : 6 }
}

Collabs = [ 'fluzz', 'ahuillet', 'jesusalva', DefaultUser ]

for collab in Collabs:
    verb = "/collaborators/{}".format(UserTranslation[collab]['name'])
    dataset = {
        'permission' : 'Write'
    }
    debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
    if not DryRun:
        req = requests.put(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
        dump_resp(req)

#====================================================================
# INIT LABELS
#====================================================================

KeywordTranslation = {
	'opengl' : { 'id' : 8 },
	'editor' : { 'id' : 9 },
	'sdl' : { 'id' : 10 },
	'dialog' : { 'id' : 11 },
	'mapgen' : { 'id' : 12 },
	'quest' : { 'id' : 13 },
	'map' : { 'id' : 14 },
	'graphics' : { 'id' : 15 },
	'takeover' : { 'id' : 16 },
	'translation' : { 'id' : 17 },
	'documentation' : { 'id' : 18 }
}

# Priority is transformed into a keyword
PriorityTranslation = {
	'critical' : { 'id' : 19 },
	'important' : { 'id' : 20 },
	'bug' : { 'id' : 21 },
	'minor' : { 'id' : 22 },
	'feature' : { 'id' : 23 },
	'idea' : { 'id' : 24 },
	'release-blocker' : { 'id' : 25 },
	'0.16 rc bugs' : { 'id' : 26 }
}

for (name, label) in KeywordTranslation.items():
    verb = "/labels"
    dataset = {
        'color' : '#0000ff',
        'name' : name
    }
    debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
    if not DryRun:
        req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
        dump_resp(req)

for (name, label) in PriorityTranslation.items():
    verb = "/labels"
    dataset = {
        'color' : '#ffff00',
        'name' : name
    }
    debug(Debug.UPLOAD, "{} : {}".format(verb, json.dumps(dataset)))
    if not DryRun:
        req = requests.post(webhookUrl + verb, data=json.dumps(dataset), headers={'Content-type' : 'application/json', 'Authorization' : 'token '+api_token})
        dump_resp(req)

